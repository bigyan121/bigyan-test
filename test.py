from flask import Flask
from flask import request

app = Flask(__name__)
#Comments
@app.route('/')
def hello():
    print(request.headers)

@app.route('/health')
def health():
    return '{status: up}'
@app.route('/greet')
def greet():
    return "Good Morning"
@app.route('/sleep')
def sleep():
    return "Good Night"

if __name__=='__main__':
    app.run()
