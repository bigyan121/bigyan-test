FROM python:alpine
LABEL MAINTAINER 'bigyan.k.c@adp.com'

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY test.py /opt/

ENV FLASK_APP=/opt/test.py

ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]